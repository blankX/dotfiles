# blank X's Dotfiles

My dotfiles. I manage them with a program I made myself... totally not recreating [amolith's dotfiles' README](https://git.nixnet.xyz/Amolith/dotfiles/src/branch/master/README.md)

# Setup
 * **Distro:** [Linux Mint](https://linuxmint.com)
 * **GTK theme:** [Canvas](https://www.opendesktop.org/p/1279224/)
 * **Icon Theme:** [New FlaT](https://www.opendesktop.org/p/1279482/)
 * **terminal:** [kitty](https://sw.kovidgoyal.net/kitty/)
 * **Shell:** [zsh](https://zsh.org)
 * **Text Editor:** [nano](https://www.nano-editor.org/) *(no judging please)*
 * **Bar:** [i3blocks](https://github.com/vivien/i3blocks)
 * **Fonts:** [Rounded Elegance](https://www.dafont.com/rounded-elegance.font) *(mostly because Telegram doesn't like to use Rounded Elegance)*
 * **WM:** [i3-gaps](https://github.com/Airblader/i3)
 * **Launcher:** [rofi](https://github.com/DaveDavenport/rofi)
 * **Lock Screen:** [i3lock](https://github.com/i3/i3lock)
 * **Screenshot Tool:** [flameshot](https://github.com/lupoDharkael/flameshot)
 * **Notification Daemon:** [xfce4-notifyd](https://git.xfce.org/apps/xfce4-notifyd)
 * **File Manager:** [dolphin](https://userbase.kde.org/Dolphin)
 * **Browser:** Usually [firefox](https://www.mozilla.org/en-US/firefox/new/), but [links2](http://atrey.karlin.mff.cuni.cz/~clock/twibright/links) sometimes

# Notes
 * Credit to [Amolith](https://keybase.io/amolith) for his `kolours` script (and the README to recreate)
 * Yes, I did copy [Amolith's dotfiles README](https://git.nixnet.xyz/Amolith/dotfiles/src/branch/master/README.md), 1000 points for originality please.
 * I'm using [antigen](https://github.com/zsh-users/antigen), but I'm not sure if I should add antigen.zsh in the repo, tell me if I should :p
