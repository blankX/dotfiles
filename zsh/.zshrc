#export TERM="xterm-256color"
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt beep
unsetopt appendhistory
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/blank/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

setopt COMPLETE_ALIASES

if [ -f ~/.zsh_aliases ]; then
    . ~/.zsh_aliases
fi

autoload -Uz promptinit
promptinit
zstyle ':completion:*' rehash true

export SUDO_PROMPT="%p@%h Password: "
export GIT_ASKPASS='/usr/bin/ssh-askpass'
export TZ='Asia/Phnom_Penh'
export EDITOR='/bin/nano'
export SUDO_EDITOR=${EDITOR}
#export PATH=/home/blank/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/home/blank/.local/kitty.app/bin
export SHELL=/usr/bin/zsh
export TODOTXT_DEFAULT_ACTION=ls
export MANPATH=/usr/local/man:/usr/local/share/man:/usr/share/man:/home/blank/.local/kitty.app/share/man:/home/blank/.local/mans
export LESS='-R'
export LESSOPEN='|~/.lessfilter %s'

source ~/.zcripts/antigen.zsh
antigen bundle paulmelnikow/zsh-startup-timer
export POWERLEVEL9K_MODE='nerdfont-complete'
antigen theme bhilburn/powerlevel9k powerlevel9k
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle rupa/z
antigen bundle djui/alias-tips
antigen bundle MikeDacre/careful_rm
antigen bundle zuxfoucault/colored-man-pages_mod
antigen bundle marzocchi/zsh-notify
antigen apply

#export POWERLEVEL9K_HOME_ICON=''
#export POWERLEVEL9K_HOME_SUB_ICON=''
#export POWERLEVEL9K_FOLDER_ICON=''
export POWERLEVEL9K_ETC_ICON=''
export POWERLEVEL9K_PROMPT_ON_NEWLINE=true
export POWERLEVEL9K_PROMPT_ADD_NEWLINE=true
export POWERLEVEL9K_CONTEXT_DEFAULT_BACKGROUND=steelblue1
export POWERLEVEL9K_CONTEXT_DEFAULT_FOREGROUND=black
powerlevel9k_custom_todo_num() {
	NUM=$(cat ~/.todo/todo.txt|wc -l)
	if [ "${NUM}" = "0" ]
	then
		exit
	else
		echo ${NUM}
	fi
}
export POWERLEVEL9K_CUSTOM_TODO_NUM="powerlevel9k_custom_todo_num"
export POWERLEVEL9K_CUSTOM_TODO_NUM_BACKGROUND=darkblue
export POWERLEVEL9K_CUSTOM_TODO_NUM_FOREGROUND=steelblue
powerlevel9k_custom_zshins() {
	FZSHINS=$(ps -C zsh|wc -l)
	ZSHINS=$((${FZSHINS}-4))
	if [ "${ZSHINS}" = "1" ]
	then
		exit
	else
		echo ${ZSHINS}
	fi
}
export POWERLEVEL9K_CUSTOM_ZSHINS='powerlevel9k_custom_zshins'
export POWERLEVEL9K_CUSTOM_ZSHINS_BACKGROUND=white
export POWERLEVEL9K_CUSTOM_ZSHINS_FOREGROUND=black
powerlevel9k_custom_no_wifi() {
	YASIR=$(nmcli device wifi|grep \*)
	SIDDIQUI=$?
	[ $SIDDIQUI -ne 0 ] && echo NO WIFI
}
export POWERLEVEL9K_CUSTOM_NO_WIFI='powerlevel9k_custom_no_wifi'
export POWERLEVEL9K_CUSTOM_NO_WIFI_BACKGROUND=red
export POWERLEVEL9K_CUSTOM_NO_WIFI_FOREGROUND=white
export POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX=""
export POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="> "
export POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir_writable dir vcs)
export POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status background_jobs custom_todo_num custom_zshins custom_no_wifi)
clear
echo "blank X has unblanked from the X world"|cowsay|lolcat

wifi() {
	nmcli -p device wifi
	O=$(nmcli -t device wifi|egrep '^\*:.+$')
	I=$?
	if [ "${I}" = "0" ]
	then
		nmap -sP 192.168.43.0/24
	fi
}
update-dot() {
	REPO_LINK='/home/blank/repos/dotfiles'
	BIN_DIR='/home/blank/.local/bin'

	cp ~/.zshrc ${REPO_LINK}/zsh/.zshrc
	cp ~/.zcripts/* ${REPO_LINK}/zsh/.zcripts
	cp ~/.zsh_aliases ${REPO_LINK}/zsh/.zsh_aliases
	cp ~/.rm_recycle_home ${REPO_LINK}/zsh/.rm_recycle_home

	cp ~/.nanorc ${REPO_LINK}/nanorc/.nanorc

	cp ${BIN_DIR}/{tg_alt,lock} ${REPO_LINK}/bin
	chmod +x ${REPO_LINK}/bin/*

	cp ~/.config/i3/* ${REPO_LINK}/i3/.config/i3

	cp ~/.config/neofetch/* ${REPO_LINK}/neofetch/.config/neofetch

	cp ~/.config/youtube-dl/config ${REPO_LINK}/youtube-dl/.config/youtube-dl/config

	cp ~/.config/compton.conf ${REPO_LINK}/compton/.config/compton.conf

	cp ~/.config/rofi/config ${REPO_LINK}/rofi/.config/rofi/config

	cp ~/.i3blocks.conf ${REPO_LINK}/i3blocks/.i3blocks.conf

	cp ~/.config/dolphinrc ${REPO_LINK}/dolphinrc/.config/dolphinrc

	cp ~/.local/bin/kolours ${REPO_LINK}/bin/kolours

	cp ~/.config/kitty/* ${REPO_LINK}/kitty/.config/kitty

	cp ~/.links2/links.cfg ${REPO_LINK}/links2/.links2/links.cfg

	cp ~/.todo/config ${REPO_LINK}/todo.txt/.todo/config
}
kpupload() {
	KEYBASE_PRIV='/keybase/private/blank_x'

	mkdir -p ${KEYBASE_PRIV}/keepass-xc-db
	rsync -vhzza --progress ~/keepass_db/* ${KEYBASE_PRIV}/keepass-xc-db

	mkdir -p ${KEYBASE_PRIV}/music
	rsync -vhzza --progress ~/Music/* ${KEYBASE_PRIV}/music

	mkdir -p ${KEYBASE_PRIV}/videos
	rsync -vhzza --progress ~/Videos/* ${KEYBASE_PRIV}/videos

	mkdir -p ${KEYBASE_PRIV}/pictures
	rsync -vhzza --progress ~/Pictures/* ${KEYBASE_PRIV}/pictures

	mkdir -p ${KEYBASE_PRIV}/documents
	rsync -vhzza --progress ~/Documents/* ${KEYBASE_PRIV}/documents

	mkdir -p ${KEYBASE_PRIV}/gpgkeys
	rsync -vhzza --progress ~/random_shit/gpgkeys/* ${KEYBASE_PRIV}/gpgkeys

	mkdir -p ${KEYBASE_PRIV}/pythonfiles
	rsync -vhzza --progress ~/random_shit/pythonfiles/* ${KEYBASE_PRIV}/pythonfiles

	mkdir -p ${KEYBASE_PRIV}/bashfiles
	rsync -vhzza --progress ~/random_shit/bashfiles/* ${KEYBASE_PRIV}/bashfiles

	rsync -vhzza --progress ~/.todo/todo.txt ${KEYBASE_PRIV}
}
kpdownload() {
	KEYBASE_PRIV='/keybase/private/blank_x'

	rsync -vhzzr --progress ${KEYBASE_PRIV}/keepass-xc-db/* ~/keepass_db

	rsync -vhzzr --progress ${KEYBASE_PRIV}/music/* ~/Music

	rsync -vhzzr --progress ${KEYBASE_PRIV}/videos/* ~/Videos

	rsync -vhzzr --progress ${KEYBASE_PRIV}/pictures/* ~/Pictures

	rsync -vhzzr --progress ${KEYBASE_PRIV}/documents/* ~/Documents

	rsync -vhzzr --progress ${KEYBASE_PRIV}/gpgkeys/* ~/random_shit/gpgkeys

	rsync -vhzzr --progress ${KEYBASE_PRIV}/pythonfiles/* ~/random_shit/pythonfiles

	rsync -vhzzr --progress ${KEYBASE_PRIV}/bashfiles/* ~/random_shit/bashfiles

	rsync -vhzzr --progress ${KEYBASE_PRIV}/todo.txt ~/.todo/todo.txt
}
transfer() { if [ $# -eq 0 ]; then echo -e "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"; return 1; fi
tmpfile=$( mktemp -t transferXXX ); if tty -s; then basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g'); curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile; else curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile ; fi; cat $tmpfile; rm -f $tmpfile; }
